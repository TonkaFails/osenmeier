FROM openjdk:11
ADD ./target/omSystem.jar omSystem.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "omSystem.jar"]