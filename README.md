# SE2021

Hier ensteht/entstand das Projekt für SE II auf Basis des Projekts von SE I

###Anwendung nutzen:

1. Möglichkeit
    
    - [Webserver erreichen](http://anton-hofmeier.de)
2. Möglichkeit
    - repository klonen
   - ./startup.sh laufen lassen(empfohlen) **oder**
    - docker-compose up

###Hinweise/Tools:
- um die Anwedung lokal in Docker zu starten sollte der **db**-Ordner, **docker-compose.yml** und **startup.sh**(*optional*) ausreichen. Das Image befindet sich im Dockerhub.

- JDK: 11

- Language Level: 11

- [Dockerhub](https://hub.docker.com/repository/docker/tonkafails/sessspringside)

- Das Projekt enthält einen Integrationtest der nur mit laufender Datenbank erfolgreich ist (*postgresqldb:5432/booking*)

- Im db Order befindet sich ein SQL-Skript(*01-init.sh*) mit dem die Postgres-Datenbank initialisiert wird. Dieser db-Order wird bei dock-compose gemountet. Außerdem benutzt dieses Skript die Variablen, die in docker-compose.yml festgelegt werden.

- *buildupload.sh* ist ein Skript mit dem das Spring-Projekt via Maven verpackt wird und im Anschluss mit dockerx gebaut und ins Hub hochgeladen wird.

- *dump.sh* ist ein Skript mit dem ein Dump bzw. Abbild der "booking"-Postgres-Datenbank gemacht wird und mit tag in den dumps-Ordner verschoben wird. Aus diesem Dump ergibt/ergab sich der großteil des *01-init.sh* Skripts

- bei push wird das Dockerimage im Dockerhub erneuert bzw. "upgedated" (siehe *.gitlab-ci.yml*)


