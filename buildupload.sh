#!/bin/bash

echo "Tests will only work if database is reachable under postgresqldb:5432/booking"
echo "skip integration test?"
echo -n "y/n?"
read answer

if [ "$answer" != "${answer#[Yy]}" ] ;then

  echo"mvn clean and package + skipping tests"
  mvn clean
  mvn package -Dmaven.test.skip=true

else
  mvn clean
  mvn package
fi

echo "build latest version and upload to dockerhub?"
echo -n "y/n?"
read answer

if [ "$answer" != "${answer#[Yy]}" ] ;then

  echo "docker buildx and push to dockerhub"
  docker buildx build --platform linux/amd64,linux/arm64 -t tonkafails/sessspringside:latest --push .
else
  echo "ok :("
fi