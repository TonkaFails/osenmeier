#!/bin/bash

echo "Starte dump der booking database"
echo "sofern diese existiert"

#todays date
now=$(date +"%m_%d_%Y")

#pg_dump
pg_dump booking > bookingdump_$now.sql

mv bookingdump_$now.sql ./dumps/bookingdump_$now.sql
