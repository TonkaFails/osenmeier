--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: Adresse; Type: TABLE; Schema: public; Owner: antonmb
--

CREATE TABLE public."Adresse" (
    "AdressID" integer NOT NULL,
    "Straße" text,
    "Ort" text,
    "Land" text
);


ALTER TABLE public."Adresse" OWNER TO antonmb;

--
-- Name: Adresse_AdressID_seq; Type: SEQUENCE; Schema: public; Owner: antonmb
--

ALTER TABLE public."Adresse" ALTER COLUMN "AdressID" ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."Adresse_AdressID_seq"
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1
);


--
-- Name: Buchung; Type: TABLE; Schema: public; Owner: antonmb
--

CREATE TABLE public."Buchung" (
    "Buchungsnr" integer NOT NULL,
    "Kundennr" integer NOT NULL,
    "Zahlungsart" text,
    "Beginn" date,
    "Ende" date,
    "Hotelnr" integer NOT NULL
);


ALTER TABLE public."Buchung" OWNER TO antonmb;

--
-- Name: Flugticket; Type: TABLE; Schema: public; Owner: antonmb
--

CREATE TABLE public."Flugticket" (
    "Flugticketnr" integer NOT NULL,
    "Plätze" integer,
    "Klasse" text,
    "Einstiegsort" text,
    "Austeigsort" text,
    "Zwischenlandung" boolean
);


ALTER TABLE public."Flugticket" OWNER TO antonmb;

--
-- Name: Flugticketbuchung; Type: TABLE; Schema: public; Owner: antonmb
--

CREATE TABLE public."Flugticketbuchung" (
    "Buchungsnr" integer NOT NULL,
    "Flugticketnummer" integer NOT NULL
);


ALTER TABLE public."Flugticketbuchung" OWNER TO antonmb;

--
-- Name: Hotel; Type: TABLE; Schema: public; Owner: antonmb
--

CREATE TABLE public."Hotel" (
    "Hotelnr" integer NOT NULL,
    "Hotelname" text,
    "Adresse" integer,
    "Sterne" integer
);


ALTER TABLE public."Hotel" OWNER TO antonmb;

--
-- Name: Kunde; Type: TABLE; Schema: public; Owner: antonmb
--

CREATE TABLE public."Kunde" (
    "Kundennr" integer NOT NULL,
    "Name" text,
    "Kundenart" text,
    "E-Mail" text,
    "Kontodaten" text,
    "Adresse" integer
);


ALTER TABLE public."Kunde" OWNER TO antonmb;

--
-- Name: Kunde_Kundennr_seq; Type: SEQUENCE; Schema: public; Owner: antonmb
--

ALTER TABLE public."Kunde" ALTER COLUMN "Kundennr" ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."Kunde_Kundennr_seq"
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1
);


--
-- Name: Massage; Type: TABLE; Schema: public; Owner: antonmb
--

CREATE TABLE public."Massage" (
    "Massagenr" integer NOT NULL,
    "Massagetyp" text,
    "Dauer" time without time zone
);


ALTER TABLE public."Massage" OWNER TO antonmb;

--
-- Name: Massagebuchung; Type: TABLE; Schema: public; Owner: antonmb
--

CREATE TABLE public."Massagebuchung" (
    "Buchungsnr" integer NOT NULL,
    "Massagenr" integer NOT NULL
);


ALTER TABLE public."Massagebuchung" OWNER TO antonmb;

--
-- Name: Mietwagen; Type: TABLE; Schema: public; Owner: antonmb
--

CREATE TABLE public."Mietwagen" (
    "Fahrzeugnr" integer NOT NULL,
    "Marke" text,
    "Typ" text,
    km integer
);


ALTER TABLE public."Mietwagen" OWNER TO antonmb;

--
-- Name: Mietwagenbuchung; Type: TABLE; Schema: public; Owner: antonmb
--

CREATE TABLE public."Mietwagenbuchung" (
    "Buchungsnr" integer NOT NULL,
    "Mietwagennr" integer NOT NULL
);


ALTER TABLE public."Mietwagenbuchung" OWNER TO antonmb;

--
-- Name: Zimmer; Type: TABLE; Schema: public; Owner: antonmb
--

CREATE TABLE public."Zimmer" (
    "Zimmernr" integer NOT NULL,
    "Art" text,
    "Anzahl-Betten" integer,
    "Lage" text,
    "Preis" real
);


ALTER TABLE public."Zimmer" OWNER TO antonmb;

--
-- Name: Zimmer_Zimmernr_seq; Type: SEQUENCE; Schema: public; Owner: antonmb
--

ALTER TABLE public."Zimmer" ALTER COLUMN "Zimmernr" ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."Zimmer_Zimmernr_seq"
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1
);


--
-- Name: Zimmerbuchung; Type: TABLE; Schema: public; Owner: antonmb
--

CREATE TABLE public."Zimmerbuchung" (
    "Buchungsnr" integer NOT NULL,
    "Zimmernr" integer NOT NULL
);


ALTER TABLE public."Zimmerbuchung" OWNER TO antonmb;

--
-- Data for Name: Adresse; Type: TABLE DATA; Schema: public; Owner: antonmb
--

COPY public."Adresse" ("AdressID", "Straße", "Ort", "Land") FROM stdin;
0	Beispielstraße	Ortschaft	Land
1	5th-Street	Bigcity	MyHomeCountry
2	falls	\N	\N
3		\N	\N
4	Waldstr	pullach	germany
5	Waldstr	pullach	germany
\.


--
-- Data for Name: Buchung; Type: TABLE DATA; Schema: public; Owner: antonmb
--

COPY public."Buchung" ("Buchungsnr", "Kundennr", "Zahlungsart", "Beginn", "Ende", "Hotelnr") FROM stdin;
\.


--
-- Data for Name: Flugticket; Type: TABLE DATA; Schema: public; Owner: antonmb
--

COPY public."Flugticket" ("Flugticketnr", "Plätze", "Klasse", "Einstiegsort", "Austeigsort", "Zwischenlandung") FROM stdin;
\.


--
-- Data for Name: Flugticketbuchung; Type: TABLE DATA; Schema: public; Owner: antonmb
--

COPY public."Flugticketbuchung" ("Buchungsnr", "Flugticketnummer") FROM stdin;
\.


--
-- Data for Name: Hotel; Type: TABLE DATA; Schema: public; Owner: antonmb
--

COPY public."Hotel" ("Hotelnr", "Hotelname", "Adresse", "Sterne") FROM stdin;
\.


--
-- Data for Name: Kunde; Type: TABLE DATA; Schema: public; Owner: antonmb
--

COPY public."Kunde" ("Kundennr", "Name", "Kundenart", "E-Mail", "Kontodaten", "Adresse") FROM stdin;
0	Klaus Kunde	Premium	klaus@ichbinreich.de	DE3544232234	0
1	Hans Hallo	Premium	klaus@ichbinreich.de	DE3544232234	0
2	Hans Hallo2	Premium	klaus@ichbinreich.de	DE3544232234	0
8	\N	\N	\N	\N	\N
9	\N	\N	\N	\N	\N
10	Hans Hallo	Premium	klaus@ichbinreich.de	DE3544232234	0
11	Hans Halloll	Premium	klaus@ichbinreich.de	DE3544232234	0
12	YEYE	gehts	\N	DE3445634524	\N
13	Anton Hofmeier	Premium	\N	DE675546452342534	\N
14	Anton Hofmeier	Premium	\N	DE675546452342534	\N
15			\N		\N
18	bb	gbvd	\N	fdfgsd	2
19	Klaus Kunde	Premium	\N	DE3544232234"	3
20	Testing Kunde	Premium	\N	DE4345476	4
21	Testing Kunde	Premium	\N	DE4345476	5
\.


--
-- Data for Name: Massage; Type: TABLE DATA; Schema: public; Owner: antonmb
--

COPY public."Massage" ("Massagenr", "Massagetyp", "Dauer") FROM stdin;
\.


--
-- Data for Name: Massagebuchung; Type: TABLE DATA; Schema: public; Owner: antonmb
--

COPY public."Massagebuchung" ("Buchungsnr", "Massagenr") FROM stdin;
\.


--
-- Data for Name: Mietwagen; Type: TABLE DATA; Schema: public; Owner: antonmb
--

COPY public."Mietwagen" ("Fahrzeugnr", "Marke", "Typ", km) FROM stdin;
\.


--
-- Data for Name: Mietwagenbuchung; Type: TABLE DATA; Schema: public; Owner: antonmb
--

COPY public."Mietwagenbuchung" ("Buchungsnr", "Mietwagennr") FROM stdin;
\.


--
-- Data for Name: Zimmer; Type: TABLE DATA; Schema: public; Owner: antonmb
--

COPY public."Zimmer" ("Zimmernr", "Art", "Anzahl-Betten", "Lage", "Preis") FROM stdin;
3	Doppelzimmer	\N	Rückseite	90
4	Doppelzimmer	\N	Rückseite	90
10	Einzel	\N	Westen	50
\.


--
-- Data for Name: Zimmerbuchung; Type: TABLE DATA; Schema: public; Owner: antonmb
--

COPY public."Zimmerbuchung" ("Buchungsnr", "Zimmernr") FROM stdin;
\.


--
-- Name: Adresse_AdressID_seq; Type: SEQUENCE SET; Schema: public; Owner: antonmb
--

SELECT pg_catalog.setval('public."Adresse_AdressID_seq"', 5, true);


--
-- Name: Kunde_Kundennr_seq; Type: SEQUENCE SET; Schema: public; Owner: antonmb
--

SELECT pg_catalog.setval('public."Kunde_Kundennr_seq"', 21, true);


--
-- Name: Zimmer_Zimmernr_seq; Type: SEQUENCE SET; Schema: public; Owner: antonmb
--

SELECT pg_catalog.setval('public."Zimmer_Zimmernr_seq"', 10, true);


--
-- Name: Adresse Adresse_pkey; Type: CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Adresse"
    ADD CONSTRAINT "Adresse_pkey" PRIMARY KEY ("AdressID");


--
-- Name: Buchung Buchung_pkey; Type: CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Buchung"
    ADD CONSTRAINT "Buchung_pkey" PRIMARY KEY ("Buchungsnr");


--
-- Name: Flugticket Flugticket_pkey; Type: CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Flugticket"
    ADD CONSTRAINT "Flugticket_pkey" PRIMARY KEY ("Flugticketnr");


--
-- Name: Flugticketbuchung Flugticketbuchung_pkey; Type: CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Flugticketbuchung"
    ADD CONSTRAINT "Flugticketbuchung_pkey" PRIMARY KEY ("Buchungsnr", "Flugticketnummer");


--
-- Name: Hotel Hotel_pkey; Type: CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Hotel"
    ADD CONSTRAINT "Hotel_pkey" PRIMARY KEY ("Hotelnr");


--
-- Name: Kunde Kunde_pkey; Type: CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Kunde"
    ADD CONSTRAINT "Kunde_pkey" PRIMARY KEY ("Kundennr");


--
-- Name: Massage Massage_pkey; Type: CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Massage"
    ADD CONSTRAINT "Massage_pkey" PRIMARY KEY ("Massagenr");


--
-- Name: Massagebuchung Massagebuchung_pkey; Type: CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Massagebuchung"
    ADD CONSTRAINT "Massagebuchung_pkey" PRIMARY KEY ("Buchungsnr", "Massagenr");


--
-- Name: Mietwagen Mietwagen_pkey; Type: CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Mietwagen"
    ADD CONSTRAINT "Mietwagen_pkey" PRIMARY KEY ("Fahrzeugnr");


--
-- Name: Mietwagenbuchung Mietwagenbuchung_pkey; Type: CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Mietwagenbuchung"
    ADD CONSTRAINT "Mietwagenbuchung_pkey" PRIMARY KEY ("Buchungsnr");


--
-- Name: Zimmerbuchung Zimmerbuchung_pkey; Type: CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Zimmerbuchung"
    ADD CONSTRAINT "Zimmerbuchung_pkey" PRIMARY KEY ("Buchungsnr", "Zimmernr");


--
-- Name: Zimmer Zimmernr; Type: CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Zimmer"
    ADD CONSTRAINT "Zimmernr" PRIMARY KEY ("Zimmernr");


--
-- Name: Kunde AdressID; Type: FK CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Kunde"
    ADD CONSTRAINT "AdressID" FOREIGN KEY ("Adresse") REFERENCES public."Adresse"("AdressID");


--
-- Name: Hotel AdressID; Type: FK CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Hotel"
    ADD CONSTRAINT "AdressID" FOREIGN KEY ("Hotelnr") REFERENCES public."Adresse"("AdressID");


--
-- Name: Zimmerbuchung Buchungsnr; Type: FK CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Zimmerbuchung"
    ADD CONSTRAINT "Buchungsnr" FOREIGN KEY ("Buchungsnr") REFERENCES public."Buchung"("Buchungsnr");


--
-- Name: Flugticketbuchung Buchungsnr; Type: FK CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Flugticketbuchung"
    ADD CONSTRAINT "Buchungsnr" FOREIGN KEY ("Buchungsnr") REFERENCES public."Buchung"("Buchungsnr");


--
-- Name: Massagebuchung Buchungsnr; Type: FK CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Massagebuchung"
    ADD CONSTRAINT "Buchungsnr" FOREIGN KEY ("Buchungsnr") REFERENCES public."Buchung"("Buchungsnr");


--
-- Name: Mietwagenbuchung Buchungsnr; Type: FK CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Mietwagenbuchung"
    ADD CONSTRAINT "Buchungsnr" FOREIGN KEY ("Buchungsnr") REFERENCES public."Buchung"("Buchungsnr");


--
-- Name: Flugticketbuchung Flugticketnr; Type: FK CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Flugticketbuchung"
    ADD CONSTRAINT "Flugticketnr" FOREIGN KEY ("Buchungsnr") REFERENCES public."Flugticket"("Flugticketnr") NOT VALID;


--
-- Name: Buchung Hotelnr; Type: FK CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Buchung"
    ADD CONSTRAINT "Hotelnr" FOREIGN KEY ("Buchungsnr") REFERENCES public."Hotel"("Hotelnr") NOT VALID;


--
-- Name: Buchung Kundennr; Type: FK CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Buchung"
    ADD CONSTRAINT "Kundennr" FOREIGN KEY ("Buchungsnr") REFERENCES public."Kunde"("Kundennr") NOT VALID;


--
-- Name: Massagebuchung Massagenr; Type: FK CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Massagebuchung"
    ADD CONSTRAINT "Massagenr" FOREIGN KEY ("Massagenr") REFERENCES public."Massage"("Massagenr");


--
-- Name: Mietwagenbuchung Mietwagennr; Type: FK CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Mietwagenbuchung"
    ADD CONSTRAINT "Mietwagennr" FOREIGN KEY ("Mietwagennr") REFERENCES public."Mietwagen"("Fahrzeugnr");


--
-- Name: Zimmerbuchung Zimmernr; Type: FK CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Zimmerbuchung"
    ADD CONSTRAINT "Zimmernr" FOREIGN KEY ("Buchungsnr") REFERENCES public."Zimmer"("Zimmernr") NOT VALID;


--
-- PostgreSQL database dump complete
--

