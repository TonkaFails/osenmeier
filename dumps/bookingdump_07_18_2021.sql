--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: Adresse; Type: TABLE; Schema: public; Owner: antonmb
--

CREATE TABLE public."Adresse" (
    "AdressID" integer NOT NULL,
    "Straße" text,
    "Ort" text,
    "Land" text
);


ALTER TABLE public."Adresse" OWNER TO antonmb;

--
-- Name: Adresse_AdressID_seq; Type: SEQUENCE; Schema: public; Owner: antonmb
--

ALTER TABLE public."Adresse" ALTER COLUMN "AdressID" ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."Adresse_AdressID_seq"
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1
);


--
-- Name: Buchung; Type: TABLE; Schema: public; Owner: antonmb
--

CREATE TABLE public."Buchung" (
    "Buchungsnr" integer NOT NULL,
    "Kundennr" integer NOT NULL,
    "Zahlungsart" text,
    "Beginn" date,
    "Ende" date,
    "Hotelnr" integer NOT NULL
);


ALTER TABLE public."Buchung" OWNER TO antonmb;

--
-- Name: Buchung_Buchungsnr_seq; Type: SEQUENCE; Schema: public; Owner: antonmb
--

ALTER TABLE public."Buchung" ALTER COLUMN "Buchungsnr" ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."Buchung_Buchungsnr_seq"
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1
);


--
-- Name: Flugticket; Type: TABLE; Schema: public; Owner: antonmb
--

CREATE TABLE public."Flugticket" (
    "Flugticketnr" integer NOT NULL,
    "Plätze" integer,
    "Klasse" text,
    "Einstiegsort" text,
    "Austeigsort" text,
    "Zwischenlandung" boolean
);


ALTER TABLE public."Flugticket" OWNER TO antonmb;

--
-- Name: Flugticketbuchung; Type: TABLE; Schema: public; Owner: antonmb
--

CREATE TABLE public."Flugticketbuchung" (
    "Buchungsnr" integer NOT NULL,
    "Flugticketnummer" integer NOT NULL
);


ALTER TABLE public."Flugticketbuchung" OWNER TO antonmb;

--
-- Name: Hotel; Type: TABLE; Schema: public; Owner: antonmb
--

CREATE TABLE public."Hotel" (
    "Hotelnr" integer NOT NULL,
    "Hotelname" text,
    "Adresse" integer,
    "Sterne" integer
);


ALTER TABLE public."Hotel" OWNER TO antonmb;

--
-- Name: Hotel_Hotelnr_seq; Type: SEQUENCE; Schema: public; Owner: antonmb
--

ALTER TABLE public."Hotel" ALTER COLUMN "Hotelnr" ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."Hotel_Hotelnr_seq"
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1
);


--
-- Name: Kunde; Type: TABLE; Schema: public; Owner: antonmb
--

CREATE TABLE public."Kunde" (
    "Kundennr" integer NOT NULL,
    "Name" text,
    "Kundenart" text,
    "Email" text,
    "Kontodaten" text,
    "Adresse" integer
);


ALTER TABLE public."Kunde" OWNER TO antonmb;

--
-- Name: Kunde_Kundennr_seq; Type: SEQUENCE; Schema: public; Owner: antonmb
--

ALTER TABLE public."Kunde" ALTER COLUMN "Kundennr" ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."Kunde_Kundennr_seq"
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1
);


--
-- Name: Mietwagen; Type: TABLE; Schema: public; Owner: antonmb
--

CREATE TABLE public."Mietwagen" (
    "Fahrzeugnr" integer NOT NULL,
    "Marke" text,
    "Typ" text,
    km integer
);


ALTER TABLE public."Mietwagen" OWNER TO antonmb;

--
-- Name: Mietwagenbuchung; Type: TABLE; Schema: public; Owner: antonmb
--

CREATE TABLE public."Mietwagenbuchung" (
    "Buchungsnr" integer NOT NULL,
    "Mietwagennr" integer NOT NULL
);


ALTER TABLE public."Mietwagenbuchung" OWNER TO antonmb;

--
-- Name: Wellness; Type: TABLE; Schema: public; Owner: antonmb
--

CREATE TABLE public."Wellness" (
    "Wellnessnr" integer NOT NULL,
    "Massagetyp" text,
    "Dauer" time without time zone
);


ALTER TABLE public."Wellness" OWNER TO antonmb;

--
-- Name: WellnessBuchung; Type: TABLE; Schema: public; Owner: antonmb
--

CREATE TABLE public."WellnessBuchung" (
    "Buchungsnr" integer NOT NULL,
    "Wellnessnr" integer NOT NULL
);


ALTER TABLE public."WellnessBuchung" OWNER TO antonmb;

--
-- Name: Wellness_Wellnessnr_seq; Type: SEQUENCE; Schema: public; Owner: antonmb
--

ALTER TABLE public."Wellness" ALTER COLUMN "Wellnessnr" ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."Wellness_Wellnessnr_seq"
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1
);


--
-- Name: Zimmer; Type: TABLE; Schema: public; Owner: antonmb
--

CREATE TABLE public."Zimmer" (
    "Zimmernr" integer NOT NULL,
    "Art" text,
    "Anzahl-Betten" integer,
    "Lage" text,
    "Preis" real
);


ALTER TABLE public."Zimmer" OWNER TO antonmb;

--
-- Name: Zimmer_Zimmernr_seq; Type: SEQUENCE; Schema: public; Owner: antonmb
--

ALTER TABLE public."Zimmer" ALTER COLUMN "Zimmernr" ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."Zimmer_Zimmernr_seq"
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1
);


--
-- Name: Zimmerbuchung; Type: TABLE; Schema: public; Owner: antonmb
--

CREATE TABLE public."Zimmerbuchung" (
    "Buchungsnr" integer NOT NULL,
    "Zimmernr" integer NOT NULL,
    "Positionsnummer" integer NOT NULL
);


ALTER TABLE public."Zimmerbuchung" OWNER TO antonmb;

--
-- Name: Zimmerbuchung_Positionsnummer_seq; Type: SEQUENCE; Schema: public; Owner: antonmb
--

ALTER TABLE public."Zimmerbuchung" ALTER COLUMN "Positionsnummer" ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."Zimmerbuchung_Positionsnummer_seq"
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1
);


--
-- Data for Name: Adresse; Type: TABLE DATA; Schema: public; Owner: antonmb
--

COPY public."Adresse" ("AdressID", "Straße", "Ort", "Land") FROM stdin;
31	Waldstraße 20	Pullach	Deutschland
32	Kirchweg 33	Bayreuth	Deutschland
33	wichtigtuerstraße 2	Köln	Deutschland
34	wichtigtuerstraße 2	Köln	Deutschland
35	wichtigtuerstraße 2	Köln	Deutschland
36	wichtigtuerstraße 1	Köln	Deutschland
37	Donnersbergerbrücke 1	München	Deutschland
38	Baustelle 234	München	Deutschland
39	Kasernenweg 90	Kiel	Deutschland
40	Fliegender-Holländerstraße 6	Amsterdam	Niederlande
41	Allgäuerstraße 80	Bremen	Deutschland
42	Graf-Zeppelin-Straße	Calw	Deutschland
43	The White House	Washington D.C.	USA
44	Friedhof	Havanna	Cuba
45	Irrerstraße 14	Nürnberg	Deutschland
46	Av. 28 de Julio 4414-4407	Lima	Peru
\.


--
-- Data for Name: Buchung; Type: TABLE DATA; Schema: public; Owner: antonmb
--

COPY public."Buchung" ("Buchungsnr", "Kundennr", "Zahlungsart", "Beginn", "Ende", "Hotelnr") FROM stdin;
0	48	MasterCard	2019-06-28	2019-07-18	0
1	49	Visa	2020-04-08	2020-04-15	0
2	60	Staatsanleihen	2021-07-08	2021-08-15	0
3	57	PayPal	2021-07-16	2021-07-30	0
\.


--
-- Data for Name: Flugticket; Type: TABLE DATA; Schema: public; Owner: antonmb
--

COPY public."Flugticket" ("Flugticketnr", "Plätze", "Klasse", "Einstiegsort", "Austeigsort", "Zwischenlandung") FROM stdin;
\.


--
-- Data for Name: Flugticketbuchung; Type: TABLE DATA; Schema: public; Owner: antonmb
--

COPY public."Flugticketbuchung" ("Buchungsnr", "Flugticketnummer") FROM stdin;
\.


--
-- Data for Name: Hotel; Type: TABLE DATA; Schema: public; Owner: antonmb
--

COPY public."Hotel" ("Hotelnr", "Hotelname", "Adresse", "Sterne") FROM stdin;
0	Grand Hustle Hotel	45	5
1	El Hotel de la Cerveza	46	4
\.


--
-- Data for Name: Kunde; Type: TABLE DATA; Schema: public; Owner: antonmb
--

COPY public."Kunde" ("Kundennr", "Name", "Kundenart", "Email", "Kontodaten", "Adresse") FROM stdin;
48	Anton Hofmeier	Stammkunde	toni.hofmeier@email.de	DE324565745634524	31
49	Kilian Osenstäter	Stammkunde	kilian@email.de	DE9876544345	32
50	Herbert Huber	Erwachsener	herbert@familie-huber.de	DE579405603450	33
51	Maximilian Huber	Kind	maximilian@familie-huber.de	DE579405603450	34
52	Laura Sophie Huber	Erwachsener	ls@familie-huber.de	DE2342566574	35
53	Maximilian Huber	Senior	maximilian-senior@familie-huber.de	BE45689785674	36
54	Kilian Beichele	Hausverbot	ignaz@franken.de	MT35265439625	37
55	Eric Jungblood	Senior	hadern@scorpio.de	DE90569648852	38
56	Daniel Sicher	Kind	sicher@scorpio.de	DE549583946892452	39
58	Rustam Tchutchikiv	Hausverbot	schuchputzer@ergo.com	BY7896093624352	41
59	Dag Bear	Erwachsener	pwtcalw@bundeswehr.org	IR9438959238405802	42
60	Joe Biden	Senior	president@whitehouse.com	classified	43
61	Fidel Castro	Stammkunde	fidel@cuba.cu	CU3546982405	44
57	Marsl Steiger	Stammkunde	marcel@werkstattleiter	NL64096093624352	40
\.


--
-- Data for Name: Mietwagen; Type: TABLE DATA; Schema: public; Owner: antonmb
--

COPY public."Mietwagen" ("Fahrzeugnr", "Marke", "Typ", km) FROM stdin;
\.


--
-- Data for Name: Mietwagenbuchung; Type: TABLE DATA; Schema: public; Owner: antonmb
--

COPY public."Mietwagenbuchung" ("Buchungsnr", "Mietwagennr") FROM stdin;
\.


--
-- Data for Name: Wellness; Type: TABLE DATA; Schema: public; Owner: antonmb
--

COPY public."Wellness" ("Wellnessnr", "Massagetyp", "Dauer") FROM stdin;
\.


--
-- Data for Name: WellnessBuchung; Type: TABLE DATA; Schema: public; Owner: antonmb
--

COPY public."WellnessBuchung" ("Buchungsnr", "Wellnessnr") FROM stdin;
\.


--
-- Data for Name: Zimmer; Type: TABLE DATA; Schema: public; Owner: antonmb
--

COPY public."Zimmer" ("Zimmernr", "Art", "Anzahl-Betten", "Lage", "Preis") FROM stdin;
14	Einzelzimmer	1	Strandseite	80
15	Einzelzimmer	1	Strandseite	80
16	Einzelzimmer	1	Strandseite	80
17	Doppelzimmer	2	Strandseite	120
18	Doppelzimmer	2	Strandseite	120
19	Doppelzimmer	2	Strandseite	120
20	Doppelzimmer Deluxe	2	Strandseite	140
21	Doppelzimmer Deluxe	2	Strandseite	140
22	Doppelzimmer Deluxe	2	Strandseite	140
23	Junior Suite	4	Strandseite	200
24	Junior Suite	4	Strandseite	200
25	Junior Suite	4	Strandseite	200
26	Senior Suite	6	Strandseite	350
27	Senior Suite	6	Strandseite	350
28	Senior Suite	6	Strandseite	350
29	Einzelzimmer	1	Vorderseite	50
30	Einzelzimmer	1	Vorderseite	50
31	Doppelzimmer	2	Vorderseite	90
32	Doppelzimmer	2	Vorderseite	90
33	Doppelzimmer	2	Vorderseite	90
34	Doppelzimmer	2	Vorderseite	90
35	Einzelzimmer	1	Panoramaseite	100
36	Einzelzimmer	1	Panoramaseite	100
37	Doppelzimmer	2	Panoramaseite	130
38	Doppelzimmer	2	Panoramaseite	130
39	Doppelzimmer	2	Panoramaseite	130
40	Junior Suite	4	Panoramaseite	250
41	Junior Suite	4	Panoramaseite	250
42	Senior Suite	6	Panoramaseite	500
43	President Suite	10	Panoramaseite	1500
\.


--
-- Data for Name: Zimmerbuchung; Type: TABLE DATA; Schema: public; Owner: antonmb
--

COPY public."Zimmerbuchung" ("Buchungsnr", "Zimmernr", "Positionsnummer") FROM stdin;
0	14	0
1	17	1
2	43	2
3	40	3
\.


--
-- Name: Adresse_AdressID_seq; Type: SEQUENCE SET; Schema: public; Owner: antonmb
--

SELECT pg_catalog.setval('public."Adresse_AdressID_seq"', 46, true);


--
-- Name: Buchung_Buchungsnr_seq; Type: SEQUENCE SET; Schema: public; Owner: antonmb
--

SELECT pg_catalog.setval('public."Buchung_Buchungsnr_seq"', 3, true);


--
-- Name: Hotel_Hotelnr_seq; Type: SEQUENCE SET; Schema: public; Owner: antonmb
--

SELECT pg_catalog.setval('public."Hotel_Hotelnr_seq"', 1, true);


--
-- Name: Kunde_Kundennr_seq; Type: SEQUENCE SET; Schema: public; Owner: antonmb
--

SELECT pg_catalog.setval('public."Kunde_Kundennr_seq"', 61, true);


--
-- Name: Wellness_Wellnessnr_seq; Type: SEQUENCE SET; Schema: public; Owner: antonmb
--

SELECT pg_catalog.setval('public."Wellness_Wellnessnr_seq"', 0, false);


--
-- Name: Zimmer_Zimmernr_seq; Type: SEQUENCE SET; Schema: public; Owner: antonmb
--

SELECT pg_catalog.setval('public."Zimmer_Zimmernr_seq"', 43, true);


--
-- Name: Zimmerbuchung_Positionsnummer_seq; Type: SEQUENCE SET; Schema: public; Owner: antonmb
--

SELECT pg_catalog.setval('public."Zimmerbuchung_Positionsnummer_seq"', 3, true);


--
-- Name: Adresse Adresse_pkey; Type: CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Adresse"
    ADD CONSTRAINT "Adresse_pkey" PRIMARY KEY ("AdressID");


--
-- Name: Buchung Buchung_pkey; Type: CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Buchung"
    ADD CONSTRAINT "Buchung_pkey" PRIMARY KEY ("Buchungsnr");


--
-- Name: Flugticket Flugticket_pkey; Type: CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Flugticket"
    ADD CONSTRAINT "Flugticket_pkey" PRIMARY KEY ("Flugticketnr");


--
-- Name: Flugticketbuchung Flugticketbuchung_pkey; Type: CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Flugticketbuchung"
    ADD CONSTRAINT "Flugticketbuchung_pkey" PRIMARY KEY ("Buchungsnr", "Flugticketnummer");


--
-- Name: Hotel Hotel_pkey; Type: CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Hotel"
    ADD CONSTRAINT "Hotel_pkey" PRIMARY KEY ("Hotelnr");


--
-- Name: Kunde Kunde_pkey; Type: CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Kunde"
    ADD CONSTRAINT "Kunde_pkey" PRIMARY KEY ("Kundennr");


--
-- Name: WellnessBuchung Massagebuchung_pkey; Type: CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."WellnessBuchung"
    ADD CONSTRAINT "Massagebuchung_pkey" PRIMARY KEY ("Buchungsnr", "Wellnessnr");


--
-- Name: Mietwagen Mietwagen_pkey; Type: CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Mietwagen"
    ADD CONSTRAINT "Mietwagen_pkey" PRIMARY KEY ("Fahrzeugnr");


--
-- Name: Mietwagenbuchung Mietwagenbuchung_pkey; Type: CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Mietwagenbuchung"
    ADD CONSTRAINT "Mietwagenbuchung_pkey" PRIMARY KEY ("Buchungsnr");


--
-- Name: Zimmerbuchung Positionsnummer; Type: CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Zimmerbuchung"
    ADD CONSTRAINT "Positionsnummer" PRIMARY KEY ("Positionsnummer");


--
-- Name: Wellness Wellness_pkey; Type: CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Wellness"
    ADD CONSTRAINT "Wellness_pkey" PRIMARY KEY ("Wellnessnr");


--
-- Name: Zimmer Zimmernr; Type: CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Zimmer"
    ADD CONSTRAINT "Zimmernr" PRIMARY KEY ("Zimmernr");


--
-- Name: Kunde AdressID; Type: FK CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Kunde"
    ADD CONSTRAINT "AdressID" FOREIGN KEY ("Adresse") REFERENCES public."Adresse"("AdressID");


--
-- Name: Hotel AdressID; Type: FK CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Hotel"
    ADD CONSTRAINT "AdressID" FOREIGN KEY ("Adresse") REFERENCES public."Adresse"("AdressID") NOT VALID;


--
-- Name: Zimmerbuchung Buchungsnr; Type: FK CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Zimmerbuchung"
    ADD CONSTRAINT "Buchungsnr" FOREIGN KEY ("Buchungsnr") REFERENCES public."Buchung"("Buchungsnr");


--
-- Name: Flugticketbuchung Buchungsnr; Type: FK CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Flugticketbuchung"
    ADD CONSTRAINT "Buchungsnr" FOREIGN KEY ("Buchungsnr") REFERENCES public."Buchung"("Buchungsnr");


--
-- Name: WellnessBuchung Buchungsnr; Type: FK CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."WellnessBuchung"
    ADD CONSTRAINT "Buchungsnr" FOREIGN KEY ("Buchungsnr") REFERENCES public."Buchung"("Buchungsnr");


--
-- Name: Mietwagenbuchung Buchungsnr; Type: FK CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Mietwagenbuchung"
    ADD CONSTRAINT "Buchungsnr" FOREIGN KEY ("Buchungsnr") REFERENCES public."Buchung"("Buchungsnr");


--
-- Name: Flugticketbuchung Flugticketnr; Type: FK CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Flugticketbuchung"
    ADD CONSTRAINT "Flugticketnr" FOREIGN KEY ("Buchungsnr") REFERENCES public."Flugticket"("Flugticketnr") NOT VALID;


--
-- Name: Buchung Hotelnr; Type: FK CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Buchung"
    ADD CONSTRAINT "Hotelnr" FOREIGN KEY ("Hotelnr") REFERENCES public."Hotel"("Hotelnr") NOT VALID;


--
-- Name: Buchung Kundennr; Type: FK CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Buchung"
    ADD CONSTRAINT "Kundennr" FOREIGN KEY ("Kundennr") REFERENCES public."Kunde"("Kundennr") NOT VALID;


--
-- Name: Mietwagenbuchung Mietwagennr; Type: FK CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Mietwagenbuchung"
    ADD CONSTRAINT "Mietwagennr" FOREIGN KEY ("Mietwagennr") REFERENCES public."Mietwagen"("Fahrzeugnr");


--
-- Name: WellnessBuchung Wellnessnr; Type: FK CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."WellnessBuchung"
    ADD CONSTRAINT "Wellnessnr" FOREIGN KEY ("Wellnessnr") REFERENCES public."Wellness"("Wellnessnr") NOT VALID;


--
-- Name: Zimmerbuchung Zimmernr; Type: FK CONSTRAINT; Schema: public; Owner: antonmb
--

ALTER TABLE ONLY public."Zimmerbuchung"
    ADD CONSTRAINT "Zimmernr" FOREIGN KEY ("Zimmernr") REFERENCES public."Zimmer"("Zimmernr") NOT VALID;


--
-- PostgreSQL database dump complete
--

