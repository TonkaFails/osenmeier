package de.ah.sews;

import de.ah.sews.entity.AdresseEntity;
import de.ah.sews.entity.GuestEntity;
import de.ah.sews.repository.GuestRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Guestconfig {

    CommandLineRunner commandLineRunner(GuestRepository repository){
        return args -> {
            GuestEntity gaster = new GuestEntity(
                    "Gast1",
                    "Luxus",
                    "mail@mail.de",
                    "DE2343652413",
                    new AdresseEntity()
            );
            repository.save(gaster);
        };
    }
}
