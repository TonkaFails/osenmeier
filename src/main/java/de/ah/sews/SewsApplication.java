package de.ah.sews;

import de.ah.sews.service.Database;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SewsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SewsApplication.class, args);
		//Database.run();
	}

}
