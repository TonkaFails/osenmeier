package de.ah.sews.controller;

import de.ah.sews.entity.AdresseEntity;
import de.ah.sews.entity.GuestEntity;
import de.ah.sews.service.AdressService;
import de.ah.sews.service.GuestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//method = RequestMethod.GET,
//produces = MediaType.APPLICATION_JSON_VALUE

@RestController
@RequestMapping(
        path = "/adress"
)
public class AdressController {

    private final AdressService addressService;

    @Autowired
    public AdressController(AdressService addressService) {
        this.addressService = addressService;
    }

    @GetMapping
    public List<AdresseEntity> getAdress() {

        return addressService.getAdress();

    }

    /*@GetMapping
    public Optional<GuestEntity> getByID(){

        return guestService.findbyID(1);
    }*/

    @PostMapping(
            consumes = {"application/x-www-form-urlencoded;charset=UTF-8",
                MediaType.APPLICATION_JSON_VALUE}
    )
    public @ResponseBody void registerGuest(AdresseEntity newAdress){

        addressService.addNewAdress(newAdress);

    }

}
