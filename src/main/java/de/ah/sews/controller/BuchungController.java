package de.ah.sews.controller;

import de.ah.sews.entity.BuchungEntity;
import de.ah.sews.service.BuchungService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
public class BuchungController {

    private final BuchungService buchungService;

    @Autowired
    public BuchungController(BuchungService buchungService) {
        this.buchungService = buchungService;
    }

    @GetMapping("/bookings")
    public List<BuchungEntity> getBookings() {
        return buchungService.getBuchungen();
    }

    @GetMapping("/bookingsbydate")
    public List<BuchungEntity> getBookingsbyBeginn(@RequestParam String dateString) throws ParseException {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String dateStrings = format.format(new Date());
        Date date = format.parse(dateString);
        return buchungService.getBuchungBeforeBeginn(date);
    }

    @GetMapping("/bookingsbetween")
    public List<BuchungEntity> getBookingsBetween(@RequestParam String biggerThanBeginn, @RequestParam String smallerThanBeginn) throws ParseException {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date biggerThan = format.parse(biggerThanBeginn);
        Date smallerThan = format.parse(smallerThanBeginn);

        return buchungService.getBuchungenBetween(biggerThan, smallerThan);
    }

    @GetMapping("/bookingbyGuestID")
    public List<BuchungEntity> getBookingByGuestID(@RequestParam Long guestid){
        return buchungService.getBookingbyGuestID(guestid);
    }

    // Mapping aus den Bookingsfinderoptions
    @RequestMapping("/bookingsByID")
    public ModelAndView callBookingsByID(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("bookingsByID_form");
        return modelAndView;
    }

    @RequestMapping("/bookingsByDates")
    public ModelAndView callBookingsByDates(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("bookingsByDates_form");
        return modelAndView;
    }

}
