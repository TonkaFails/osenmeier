package de.ah.sews.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import de.ah.sews.entity.GuestEntity;
import de.ah.sews.service.GuestService;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;

//method = RequestMethod.GET,
//produces = MediaType.APPLICATION_JSON_VALUE

@RestController
public class GuestController {

    private final Logger logger = Logger.getLogger(this.getClass());

    private final GuestService guestService;

    @Autowired
    public GuestController(GuestService guestService) {
        this.guestService = guestService;

    }

    @GetMapping("/guest")
    public List<GuestEntity> getPerson() {

        return guestService.getGuest();

    }

    @PostMapping(
            path = "guest",
            consumes = {"application/x-www-form-urlencoded;charset=UTF-8",
                    MediaType.APPLICATION_JSON_VALUE}
    )
    public @ResponseBody
    ModelAndView registerGuest(GuestEntity newGuest, HttpServletRequest httpServletRequest) throws JsonProcessingException {

        ModelAndView modelAndView = new ModelAndView();
        if(newGuest.getName() == null || newGuest.getKontodaten() == null || newGuest.getAdresse().getStraße() == null){

            logger.debug("missing values for Entity guest -> e.g. name");
            modelAndView.addObject("error", true);
            return modelAndView;

        }
        logger.log(Level.TRACE, "Create new Guestentity");
        guestService.addNewGuest(newGuest);
        return res(Math.toIntExact(newGuest.getKundennr()));


    }

    @GetMapping("/guestdelete")
    public ModelAndView guestdelete() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("guestdelete_form");
        return modelAndView;
    }

    @RequestMapping("/guestdelete/result")
    public ModelAndView del(@RequestParam int id) {
        guestService.deletebyID(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("guestdelete_form");


        return modelAndView;
    }

    @GetMapping("/guestfinder")
    public ModelAndView getString() {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("guestfinder_form");
        return modelAndView;
    }

    @GetMapping("/guestfinder/result")
    public ModelAndView res(@RequestParam int id) throws JsonProcessingException {
        //return guestService.findbyID(id);
        ModelAndView modelAndView = new ModelAndView();
        if (guestService.findbyID(id).isEmpty()) {
            logger.debug("no databaseentry for this id:" + id);
            modelAndView.addObject("error", true);
            return modelAndView;
        }
        modelAndView.setViewName("guestres");
        List<GuestEntity> list = guestService.findbyID(id)
                .map(Collections::singletonList)
                .orElseGet(Collections::emptyList);
        for (GuestEntity guest : list) {
            System.out.println(guest.toString());
            System.out.println(guest.getName());
            modelAndView.addObject("name", guest.getName() + "\n");
            modelAndView.addObject("art", guest.getKundenart());
            modelAndView.addObject("konto", guest.getKontodaten());
            modelAndView.addObject("mail", guest.getEmail());
            modelAndView.addObject("street", guest.getAdresse().getStraße());
            modelAndView.addObject("city", guest.getAdresse().getOrt());
            modelAndView.addObject("country", guest.getAdresse().getLand());
        }
        return modelAndView;
        /*
        if(guestService.findbyID(id).isEmpty()){
            return ResponseEntity.notFound().build();
        }else{
            return ResponseEntity.ok(guestService.findbyID(id).get());
        }
        */
    }

}
