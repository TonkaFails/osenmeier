package de.ah.sews.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class WelcomeController {

    @RequestMapping("/")
    public ModelAndView callWelcome(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("welcome");
        return modelAndView;
    }

    @RequestMapping("/contact")
    public ModelAndView callContactform(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("contactform");
        return modelAndView;
    }

    @RequestMapping("/calculator")
    public ModelAndView callCalculator(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("calculator");
        return modelAndView;
    }

    @RequestMapping("/registerguest")
    public ModelAndView callRegister(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("registerguest");
        return modelAndView;
    }

    @RequestMapping("/guestdelete")
    public ModelAndView callGuestDelete(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("guestdelete_form");
        return modelAndView;
    }
    @RequestMapping("/bookingfinder")
    public ModelAndView callBookingfinder(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("bookingfinderOptions");
        return modelAndView;
    }
    @RequestMapping("/guestoptions")
    public ModelAndView callGuestOptions(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("guestfinderOptions");
        return modelAndView;
    }
}
