package de.ah.sews.controller;

import de.ah.sews.entity.ZimmerEntity;
import de.ah.sews.entity.ZimmerbuchungEntity;
import de.ah.sews.service.ZimmerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ZimmerController {

    private final ZimmerService zimmerService;

    @Autowired
    public ZimmerController(ZimmerService zimmerService){
        this.zimmerService = zimmerService;
    }

    @GetMapping("/rooms")
    public List<ZimmerEntity> getZimmer(){
        return  zimmerService.getZimmer();
    }

    @PostMapping(
            path = "rooms",
            consumes = {"application/x-www-form-urlencoded;charset=UTF-8",
                    MediaType.APPLICATION_JSON_VALUE}
    )
    public @ResponseBody void registerRoom(ZimmerEntity newRoom){

        zimmerService.addNewRoom(newRoom);

    }

    @GetMapping("findAllBooked")
    public List<ZimmerbuchungEntity> findAllBooked(){
        return zimmerService.findAllBooked();
    }
}
