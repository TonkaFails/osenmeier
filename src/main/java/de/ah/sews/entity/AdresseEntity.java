package de.ah.sews.entity;

import javax.persistence.*;

@Entity
@Table(name = "\"Adresse\"", schema = "public", catalog = "Booking")
public class AdresseEntity {
    private Integer adressId;
    private String straße;
    private String ort;
    private String land;

    @Id
    @Column(name = "\"AdressID\"")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getAdressId() {
        return adressId;
    }

    public void setAdressId(Integer adressId) {
        this.adressId = adressId;
    }

    @Basic
    @Column(name = "\"Straße\"")
    public String getStraße() {
        return straße;
    }

    public void setStraße(String straße) {
        this.straße = straße;
    }

    @Basic
    @Column(name = "\"Ort\"")
    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    @Basic
    @Column(name = "\"Land\"")
    public String getLand() {
        return land;
    }

    public void setLand(String land) {
        this.land = land;
    }

    @Override
    public String toString() {
        return "ID= " + adressId + "\n" +
                "Straße: " + straße + '\n' +
                "Ort: " + ort + '\n' +
                "Land: " + land + '\n';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AdresseEntity that = (AdresseEntity) o;

        if (adressId != that.adressId) return false;
        if (straße != null ? !straße.equals(that.straße) : that.straße != null) return false;
        if (ort != null ? !ort.equals(that.ort) : that.ort != null) return false;
        if (land != null ? !land.equals(that.land) : that.land != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = adressId;
        result = 31 * result + (straße != null ? straße.hashCode() : 0);
        result = 31 * result + (ort != null ? ort.hashCode() : 0);
        result = 31 * result + (land != null ? land.hashCode() : 0);
        return result;
    }


}
