package de.ah.sews.entity;

import javax.persistence.*;
import javax.swing.*;
import java.util.Date;


@Entity
@Table(name = "\"Buchung\"", schema = "public", catalog = "booking")
public class BuchungEntity {
    private Integer buchungsnr;
    private String zahlungsart;
    private Date beginn;
    private Date ende;
    private HotelEntity hotel;
    private GuestEntity kunde;

    public BuchungEntity() {

    }

    public BuchungEntity(Integer buchungsnr, String zahlungsart, Date beginn, Date ende, HotelEntity hotel, GuestEntity kunde) {
        this.buchungsnr = buchungsnr;
        this.zahlungsart = zahlungsart;
        this.beginn = beginn;
        this.ende = ende;
        this.hotel = hotel;
        this.kunde = kunde;
    }

    @Id
    @Column(name = "\"Buchungsnr\"", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getBuchungsnr() {
        return buchungsnr;
    }

    public void setBuchungsnr(Integer buchungsnr) {
        this.buchungsnr = buchungsnr;
    }

    @Basic
    @Column(name = "\"Zahlungsart\"", nullable = true, length = -1)
    public String getZahlungsart() {
        return zahlungsart;
    }

    public void setZahlungsart(String zahlungsart) {
        this.zahlungsart = zahlungsart;
    }

    @Basic
    @Column(name = "\"Beginn\"", nullable = true)
    @Temporal(TemporalType.DATE)
    public Date getBeginn() {
        return beginn;
    }

    public void setBeginn(Date beginn) {
        this.beginn = beginn;
    }

    @Basic
    @Column(name = "\"Ende\"", nullable = true)
    @Temporal(TemporalType.DATE)
    public Date getEnde() {
        return ende;
    }

    public void setEnde(Date ende) {
        this.ende = ende;
    }

    @OneToOne()
    @JoinColumn(name = "\"Hotelnr\"")
    public HotelEntity getHotel() {
        return hotel;
    }

    public void setHotel(HotelEntity hotel) {
        this.hotel = hotel;
    }

    @OneToOne()
    @JoinColumn(name = "\"Kundennr\"")
    public GuestEntity getKunde() {
        return kunde;
    }

    public void setKunde(GuestEntity kunde) {
        this.kunde = kunde;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BuchungEntity)) return false;

        BuchungEntity that = (BuchungEntity) o;

        if (getBuchungsnr() != null ? !getBuchungsnr().equals(that.getBuchungsnr()) : that.getBuchungsnr() != null)
            return false;
        if (getZahlungsart() != null ? !getZahlungsart().equals(that.getZahlungsart()) : that.getZahlungsart() != null)
            return false;
        if (getBeginn() != null ? !getBeginn().equals(that.getBeginn()) : that.getBeginn() != null) return false;
        if (getEnde() != null ? !getEnde().equals(that.getEnde()) : that.getEnde() != null) return false;
        if (getHotel() != null ? !getHotel().equals(that.getHotel()) : that.getHotel() != null) return false;
        return getKunde() != null ? getKunde().equals(that.getKunde()) : that.getKunde() == null;
    }

    @Override
    public int hashCode() {
        int result = getBuchungsnr() != null ? getBuchungsnr().hashCode() : 0;
        result = 31 * result + (getZahlungsart() != null ? getZahlungsart().hashCode() : 0);
        result = 31 * result + (getBeginn() != null ? getBeginn().hashCode() : 0);
        result = 31 * result + (getEnde() != null ? getEnde().hashCode() : 0);
        result = 31 * result + (getHotel() != null ? getHotel().hashCode() : 0);
        result = 31 * result + (getKunde() != null ? getKunde().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "BuchungEntity{" +
                "buchungsnr=" + buchungsnr +
                ", zahlungsart='" + zahlungsart + '\'' +
                ", beginn=" + beginn +
                ", ende=" + ende +
                ", hotel=" + hotel +
                ", kunde=" + kunde +
                '}';
    }
}
