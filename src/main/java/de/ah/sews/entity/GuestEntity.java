package de.ah.sews.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "\"Kunde\"", schema = "public", catalog = "booking")
public class GuestEntity {
    private Long kundennr;
    private String name;
    private String kundenart;
    private String eMail;
    private String kontodaten;
    private AdresseEntity adresse;

    public GuestEntity() {

    }

    @Id
    @Column(name = "\"Kundennr\"")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getKundennr() {
        return kundennr;
    }

    public void setKundennr(Long kundennr) {
        this.kundennr = kundennr;
    }

    @Basic
    @Column(name = "\"Name\"")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "\"Kundenart\"")
    public String getKundenart() {
        return kundenart;
    }

    public void setKundenart(String kundenart) {
        this.kundenart = kundenart;
    }

    @Basic
    @Column(name = "\"Email\"")
    public String getEmail() {
        return eMail;
    }

    public void setEmail(String eMail) {
        this.eMail = eMail;
    }

    @Basic
    @Column(name = "\"Kontodaten\"")
    public String getKontodaten() {
        return kontodaten;
    }

    public void setKontodaten(String kontodaten) {
        this.kontodaten = kontodaten;
    }

    @OneToOne()
    @JoinColumn(name = "\"Adresse\"")
    public AdresseEntity getAdresse() {
        return adresse;
    }

    public void setAdresse(AdresseEntity adresse) {
        this.adresse = adresse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GuestEntity that = (GuestEntity) o;

        if (kundennr != that.kundennr) return false;
        if (!Objects.equals(name, that.name)) return false;
        if (!Objects.equals(kundenart, that.kundenart)) return false;
        if (!Objects.equals(eMail, that.eMail)) return false;
        if (!Objects.equals(kontodaten, that.kontodaten)) return false;
        if (!Objects.equals(adresse, that.adresse)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = Math.toIntExact(kundennr);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (kundenart != null ? kundenart.hashCode() : 0);
        result = 31 * result + (eMail != null ? eMail.hashCode() : 0);
        result = 31 * result + (kontodaten != null ? kontodaten.hashCode() : 0);
        result = 31 * result + (adresse != null ? adresse.hashCode() : 0);
        return result;
    }

    public GuestEntity(String name, String kundenart, String eMail, String kontodaten, AdresseEntity adresse) {
        this.name = name;
        this.kundenart = kundenart;
        this.eMail = eMail;
        this.kontodaten = kontodaten;
        this.adresse = adresse;
    }

    @Override
    public String toString() {
        return "GuestEntity{" +
                "kundennr=" + kundennr +
                ", name='" + name + '\'' +
                ", kundenart='" + kundenart + '\'' +
                ", eMail='" + eMail + '\'' +
                ", kontodaten='" + kontodaten + '\'' +
                ", adresse=" + adresse +
                '}';
    }
}
