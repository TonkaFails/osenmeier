package de.ah.sews.entity;

import javax.persistence.*;

@Entity
@Table(name = "\"Hotel\"", schema = "public", catalog = "booking")
public class HotelEntity {
    private Long hotelnr;
    private String hotelname;
    private Integer sterne;
    private AdresseEntity adresse;

    @Id
    @Column(name = "\"Hotelnr\"")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getHotelnr() {
        return hotelnr;
    }

    public void setHotelnr(Long hotelnr) {
        this.hotelnr = hotelnr;
    }

    @Basic
    @Column(name = "\"Hotelname\"")
    public String getHotelname() {
        return hotelname;
    }

    public void setHotelname(String hotelname) {
        this.hotelname = hotelname;
    }

    @Basic
    @Column(name = "\"Sterne\"")
    public Integer getSterne() {
        return sterne;
    }

    public void setSterne(Integer sterne) {
        this.sterne = sterne;
    }

    @OneToOne()
    @JoinColumn(name = "\"Adresse\"")
    public AdresseEntity getAdresse() {
        return adresse;
    }

    public void setAdresse(AdresseEntity adresse) {
        this.adresse = adresse;
    }

    public HotelEntity(){

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HotelEntity that = (HotelEntity) o;

        if (hotelnr != that.hotelnr) return false;
        if (hotelname != null ? !hotelname.equals(that.hotelname) : that.hotelname != null) return false;
        if (sterne != null ? !sterne.equals(that.sterne) : that.sterne != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = Math.toIntExact(hotelnr);
        result = 31 * result + (hotelname != null ? hotelname.hashCode() : 0);
        result = 31 * result + (sterne != null ? sterne.hashCode() : 0);
        return result;
    }

    public HotelEntity(Long hotelnr, String hotelname, Integer sterne, AdresseEntity adresse) {
        this.hotelnr = hotelnr;
        this.hotelname = hotelname;
        this.sterne = sterne;
        this.adresse = adresse;
    }

    @Override
    public String toString() {
        return "HotelEntity{" +
                "hotelnr=" + hotelnr +
                ", hotelname='" + hotelname + '\'' +
                ", sterne=" + sterne +
                ", adresse=" + adresse +
                '}';
    }
}
