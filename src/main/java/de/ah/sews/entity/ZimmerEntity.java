package de.ah.sews.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "\"Zimmer\"", schema = "public", catalog = "Booking")
public class ZimmerEntity {
    private int zimmernr;
    private String art;
    private Integer anzahlBetten;
    private String lage;
    private Float preis;

    @Id
    @Column(name = "\"Zimmernr\"")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getZimmernr() {
        return zimmernr;
    }

    public void setZimmernr(int zimmernr) {
        this.zimmernr = zimmernr;
    }

    @Basic
    @Column(name = "\"Art\"")
    public String getArt() {
        return art;
    }

    public void setArt(String art) {
        this.art = art;
    }

    @Basic
    @Column(name = "\"Anzahl-Betten\"")
    public Integer getAnzahlBetten() {
        return anzahlBetten;
    }

    public void setAnzahlBetten(Integer anzahlBetten) {
        this.anzahlBetten = anzahlBetten;
    }

    @Basic
    @Column(name = "\"Lage\"")
    public String getLage() {
        return lage;
    }

    public void setLage(String lage) {
        this.lage = lage;
    }

    @Basic
    @Column(name = "\"Preis\"")
    public Float getPreis() {
        return preis;
    }

    public void setPreis(Float preis) {
        this.preis = preis;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ZimmerEntity)) return false;

        ZimmerEntity that = (ZimmerEntity) o;

        if (getZimmernr() != that.getZimmernr()) return false;
        if (getArt() != null ? !getArt().equals(that.getArt()) : that.getArt() != null) return false;
        if (getAnzahlBetten() != null ? !getAnzahlBetten().equals(that.getAnzahlBetten()) : that.getAnzahlBetten() != null)
            return false;
        if (getLage() != null ? !getLage().equals(that.getLage()) : that.getLage() != null) return false;
        return getPreis() != null ? getPreis().equals(that.getPreis()) : that.getPreis() == null;
    }

    @Override
    public int hashCode() {
        int result = getZimmernr();
        result = 31 * result + (getArt() != null ? getArt().hashCode() : 0);
        result = 31 * result + (getAnzahlBetten() != null ? getAnzahlBetten().hashCode() : 0);
        result = 31 * result + (getLage() != null ? getLage().hashCode() : 0);
        result = 31 * result + (getPreis() != null ? getPreis().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ZimmerEntity{" +
                "zimmernr=" + zimmernr +
                ", art='" + art + '\'' +
                ", anzahlBetten=" + anzahlBetten +
                ", lage='" + lage + '\'' +
                ", preis=" + preis +
                '}';
    }
}
