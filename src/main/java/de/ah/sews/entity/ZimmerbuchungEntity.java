package de.ah.sews.entity;

import javax.persistence.*;

@Entity
@Table(name = "\"Zimmerbuchung\"", schema = "public", catalog = "booking")
public class ZimmerbuchungEntity {
    private Integer positionsnummer;
    private BuchungEntity buchung;
    private ZimmerEntity zimmer;

    @Id
    @Column(name = "\"Positionsnummer\"", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getPositionsnummer() {
        return positionsnummer;
    }

    public void setPositionsnummer(Integer positionsnummer) {
        this.positionsnummer = positionsnummer;
    }

    @OneToOne()
    @JoinColumn(name = "\"Buchungsnr\"")
    public BuchungEntity getBuchung(){
        return buchung;
    }

    public void setBuchung(BuchungEntity buchung){
        this.buchung = buchung;
    }

    @OneToOne()
    @JoinColumn(name = "\"Zimmernr\"")
    public ZimmerEntity getZimmer(){
        return zimmer;
    }

    public void setZimmer(ZimmerEntity zimmer){
        this.zimmer = zimmer;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ZimmerbuchungEntity)) return false;

        ZimmerbuchungEntity that = (ZimmerbuchungEntity) o;

        if (getPositionsnummer() != null ? !getPositionsnummer().equals(that.getPositionsnummer()) : that.getPositionsnummer() != null)
            return false;
        if (getBuchung() != null ? !getBuchung().equals(that.getBuchung()) : that.getBuchung() != null) return false;
        return getZimmer() != null ? getZimmer().equals(that.getZimmer()) : that.getZimmer() == null;
    }

    @Override
    public int hashCode() {
        int result = getPositionsnummer() != null ? getPositionsnummer().hashCode() : 0;
        result = 31 * result + (getBuchung() != null ? getBuchung().hashCode() : 0);
        result = 31 * result + (getZimmer() != null ? getZimmer().hashCode() : 0);
        return result;
    }

    public ZimmerbuchungEntity(){

    }

    public ZimmerbuchungEntity(Integer positionsnummer, BuchungEntity buchung, ZimmerEntity zimmer) {
        this.positionsnummer = positionsnummer;
        this.buchung = buchung;
        this.zimmer = zimmer;
    }

    @Override
    public String toString() {
        return "ZimmerbuchungEntity{" +
                "positionsnummer=" + positionsnummer +
                ", buchung=" + buchung +
                ", zimmer=" + zimmer +
                '}';
    }
}
