package de.ah.sews.repository;

import de.ah.sews.entity.AdresseEntity;
import de.ah.sews.entity.GuestEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AdressRepository extends JpaRepository<AdresseEntity, Long> {

    /*@Override
    List<GuestEntity> findAll();*/

    AdresseEntity findAdresseEntityByAdressId(int adressid);

}
