package de.ah.sews.repository;

import de.ah.sews.entity.BuchungEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface BuchungRepository extends JpaRepository<BuchungEntity, Long> {

    BuchungEntity findBuchungEntityByBuchungsnr(int id);

    List<BuchungEntity> findAllByBeginn(Date beginnDate);

    List<BuchungEntity> findAllByEnde(Date endDate);

    @Query("SELECT b FROM BuchungEntity b WHERE b.beginn <= :beginnBeforeDate")
    List<BuchungEntity> findAllByBeginnBefore(@Param("beginnBeforeDate") Date beginnBeforDate);

    @Query("SELECT b FROM BuchungEntity b WHERE b.beginn <= :biggerThanBeginn AND b.beginn >= :smallerThanBeginn")
    List<BuchungEntity> findAllbyBeginnBetween(@Param("biggerThanBeginn") Date biggerThanBeginn, @Param("smallerThanBeginn") Date smallerThanBeginn);

    List<BuchungEntity> findAllByKunde_Kundennr(@Param("guestid") Long guestid);
}

