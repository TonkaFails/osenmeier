package de.ah.sews.repository;

import de.ah.sews.entity.GuestEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GuestRepository extends JpaRepository<GuestEntity, Long> {

    /*@Override
    List<GuestEntity> findAll();*/

    Optional<GuestEntity> findGuestEntityByKundennr(Long kundennr);

}
