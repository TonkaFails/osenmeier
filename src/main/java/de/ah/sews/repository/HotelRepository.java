package de.ah.sews.repository;

import de.ah.sews.entity.HotelEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HotelRepository extends JpaRepository<HotelEntity, Long> {

    HotelEntity findHotelEntityByHotelnr(Long id);
}
