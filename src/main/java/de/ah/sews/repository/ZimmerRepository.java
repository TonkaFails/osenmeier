package de.ah.sews.repository;

import de.ah.sews.entity.ZimmerEntity;
import de.ah.sews.entity.ZimmerbuchungEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ZimmerRepository extends JpaRepository<ZimmerEntity, Long> {

    Optional<ZimmerEntity> findZimmerEntityByZimmernr(int zimmernr);

    @Query("SELECT zb FROM BuchungEntity b, ZimmerEntity z, ZimmerbuchungEntity zb WHERE z = zb.zimmer AND zb.buchung = b AND b.beginn <= CURRENT_DATE AND b.ende >= CURRENT_DATE ")
    List<ZimmerbuchungEntity> findAllBooked();
}
