package de.ah.sews.repository;

import de.ah.sews.entity.ZimmerbuchungEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ZimmerbuchungRepository extends JpaRepository<ZimmerbuchungEntity, Long> {

    ZimmerbuchungEntity findZimmerbuchungEntityByPositionsnummer(int id);
}
