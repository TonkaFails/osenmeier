package de.ah.sews.service;

import de.ah.sews.entity.AdresseEntity;
import de.ah.sews.entity.GuestEntity;
import de.ah.sews.repository.AdressRepository;
import de.ah.sews.repository.GuestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AdressService {

    private final AdressRepository adressRepository;

    @Autowired
    public AdressService(AdressRepository adressRepository) {
        this.adressRepository = adressRepository;
    }

    public List<AdresseEntity> getAdress() {

        return adressRepository.findAll();

    }

    public void addNewAdress(AdresseEntity adresseEntity) {
        System.out.println(adresseEntity);
        adressRepository.save(adresseEntity);

    }

    public AdresseEntity findbyID(int id){
        return adressRepository.findAdresseEntityByAdressId(id);
    }
}
