package de.ah.sews.service;

import de.ah.sews.entity.BuchungEntity;

import de.ah.sews.repository.BuchungRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class BuchungService {

    private final BuchungRepository buchungRepository;

    @Autowired
    public BuchungService(BuchungRepository buchungRepository) {
        this.buchungRepository = buchungRepository;
    }

    public List<BuchungEntity> getBuchungen() {
        return buchungRepository.findAll();
    }

    public List<BuchungEntity> getBuchungfromBeginn(Date date){
        return buchungRepository.findAllByBeginn(date);
    }

    public List<BuchungEntity> getBuchungBeforeBeginn(Date date){
        return buchungRepository.findAllByBeginnBefore(date);
    }

    public List<BuchungEntity> getBuchungenBetween(Date biggerThanBeginn, Date smallerThanBeginn){
        return buchungRepository.findAllbyBeginnBetween(biggerThanBeginn, smallerThanBeginn);
    }

    public List<BuchungEntity> getBookingbyGuestID(Long guestid) {

        return buchungRepository.findAllByKunde_Kundennr(guestid);
    }
}
