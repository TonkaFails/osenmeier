package de.ah.sews.service;

import java.sql.*;

public class Database {


    public static void main(String[] args) {
        Database.run();
    }

    public static void run() {

        Connection connection = null;
        String host = "localhost";
        String port = "5432";
        String db_name = "booking";
        String username = "antonmb";
        String password = "docker";

        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://" + host + ":" + port + "/" + db_name + "", "" + username + "", "" + password + "");
        } catch (ClassNotFoundException | SQLException cnfe) {
            cnfe.printStackTrace();
        }
        if (connection != null) {
            System.out.println("Connection OK");
        } else {
            System.out.println("Connection NOT OK");
            System.exit(-1);
        }

        try {
            String sql = "SELECT * from public.\"Buchung\";";
            String currentGuests = "SELECT zb.\"Buchungsnr\", zb.\"Zimmernr\", b.\"Kundennr\", k.\"Name\"\n" +
                    "\tFROM public.\"Zimmerbuchung\" zb, public.\"Zimmer\" z, public.\"Buchung\" b, public.\"Kunde\" k\n" +
                    "\tWHERE z.\"Zimmernr\" = zb.\"Zimmernr\"\n" +
                    "\tAND zb.\"Buchungsnr\" = b.\"Buchungsnr\"\n" +
                    "\tAND k.\"Kundennr\" = b.\"Kundennr\"\n" +
                    "\tAND b.\"Beginn\" <= CURRENT_DATE\n" +
                    "\tAND b.\"Ende\" >= CURRENT_DATE;";

            Statement statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery(currentGuests);
            while (resultSet.next()){
                System.out.println(resultSet.getString(2) + " "
                        + resultSet.getString(3) + " "
                        + resultSet.getString(4) + " "
                        + resultSet.getString(1));
            }


       } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*
    So findet man alle Gäste die zur Zeit im Hotel sind

    SELECT zb."Buchungsnr", zb."Zimmernr", b."Kundennr", k."Name"
	FROM public."Zimmerbuchung" zb, public."Zimmer" z, public."Buchung" b, public."Kunde" k
	WHERE z."Zimmernr" = zb."Zimmernr"
	AND zb."Buchungsnr" = b."Buchungsnr"
	AND k."Kundennr" = b."Kundennr"
	AND b."Beginn" <= CURRENT_DATE
	AND b."Ende" >= CURRENT_DATE;

	So findet man die belegten Zimmer
	SELECT zb."Buchungsnr", zb."Zimmernr"
	FROM public."Zimmerbuchung" zb, public."Zimmer" z, public."Buchung" b
	WHERE z."Zimmernr" = zb."Zimmernr"
	AND zb."Buchungsnr" = b."Buchungsnr"
	AND b."Beginn" <= CURRENT_DATE
	AND b."Ende" >= CURRENT_DATE;
     */

}
