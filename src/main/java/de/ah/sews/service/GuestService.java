package de.ah.sews.service;

import de.ah.sews.repository.AdressRepository;
import de.ah.sews.repository.GuestRepository;
import de.ah.sews.entity.GuestEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GuestService {

    private final GuestRepository guestRepository;
    private final AdressRepository adressRepository;

    @Autowired
    public GuestService(GuestRepository guestRepository, AdressRepository adressRepository) {
        this.guestRepository = guestRepository;
        this.adressRepository = adressRepository;
    }

    public List<GuestEntity> getGuest() {

        return guestRepository.findAll();

    }

    public void addNewGuest(GuestEntity newGuest) {
        adressRepository.save(newGuest.getAdresse());
        guestRepository.save(newGuest);
    }

    public Optional<GuestEntity> findbyID(int id) {

        return guestRepository.findGuestEntityByKundennr((long) id);
    }

    public void deletebyID(int id) {
        guestRepository.deleteById((long) id);
    }
}
