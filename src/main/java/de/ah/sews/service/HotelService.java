package de.ah.sews.service;

import de.ah.sews.entity.HotelEntity;
import de.ah.sews.repository.HotelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HotelService {

    private final HotelRepository hotelRepository;

    @Autowired
    public HotelService(HotelRepository hotelRepository){
        this.hotelRepository = hotelRepository;
    }

    public List<HotelEntity> getHotel(){
        return hotelRepository.findAll();
    }
}
