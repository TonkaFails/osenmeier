package de.ah.sews.service;

import de.ah.sews.entity.ZimmerEntity;
import de.ah.sews.entity.ZimmerbuchungEntity;
import de.ah.sews.repository.ZimmerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ZimmerService {

    private final ZimmerRepository zimmerRepository;

    @Autowired
    public ZimmerService(ZimmerRepository zimmerRepository){

        this.zimmerRepository = zimmerRepository;

    }

    public List<ZimmerEntity> getZimmer(){

       return zimmerRepository.findAll();
    }

    public void addNewRoom(ZimmerEntity newRoom) {

        System.out.println(newRoom);
        zimmerRepository.save(newRoom);
    }

    public List<ZimmerbuchungEntity> findAllBooked(){
        return zimmerRepository.findAllBooked();
    }
}
