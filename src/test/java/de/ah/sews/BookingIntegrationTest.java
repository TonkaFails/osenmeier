package de.ah.sews;

import de.ah.sews.entity.BuchungEntity;
import de.ah.sews.repository.BuchungRepository;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
public class BookingIntegrationTest {

    @Autowired
    private BuchungRepository buchungRepository;

    @Test
    public void returnBiden() throws ParseException {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date biggerThan = format.parse("2021-07-09");
        Date smallerThan = format.parse("2021-07-07");

        List<BuchungEntity> resultList = buchungRepository.findAllbyBeginnBetween(biggerThan, smallerThan);

        Assertions.assertEquals(1, resultList.size());
    }
}
