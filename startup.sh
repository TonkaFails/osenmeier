#!/bin/bash

echo "container werden gestoppt, gelöscht, neu runtergeladen und wieder hochgefahren"

if [ "$1" != "-y" ]; then
echo -n "y/n?"
read answer
fi

if [ "$answer" != "${answer#[Yy]}" ] || [ "$1" == "-y" ] ;then

	sudo docker-compose stop
	sudo docker-compose rm -f
	sudo docker-compose pull
	sudo docker-compose up -d
else
	echo "ok :/"
fi
